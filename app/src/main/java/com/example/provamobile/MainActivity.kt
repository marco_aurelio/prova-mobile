package com.example.provamobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var cal : Button = findViewById(R.id.button)

        var n1 : EditText = findViewById(R.id.n1)

        var n2 : EditText = findViewById(R.id.n2)

        var resultado : TextView = findViewById(R.id.resultado)



        cal.setOnClickListener {
            var calcular =  n1.text.toString().toDouble() + n2.text.toString().toDouble()
            resultado.text = resultado.text.toString() + calcular.toString()
        }




    }
}